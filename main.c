/*
 * Arel Jann Clemente | 100 Lockers, 100 Students Puzzle
 *  
 * There is currently a bug or a miscalculation that I
 * cannot seem to debug. The output displays the following open lockers: 
 *                      1 4 9 25 36 49 64 100 
 *               
 * According to many other sources and sites, the correct
 * output for the following open lockers is:  
 *                   1 4 9 16 25 36 49 64 81 100 
 * 
 * I am missing 16 and 81 as outputs and I cannot seem to figure out why.
 */

#include <stdio.h>
#include <stdbool.h>

int main() {
    bool lockers[100] = { 0 }; // boolean array for 100 lockers
    register int s, l = 1;     // initialize counters using registers
    
    // use for-loop to iterate thru every locker
    /* 
     * the key here is to multiply the s counter by the l counter
     * and change the lockers by checking if they are open or not
     */ 
    for(s = 1; s <= sizeof lockers; s++) {
        lockers[(s * l) - 1] = (!lockers[s - 1]) ? 1 : 0;
        l++;
    }
    
    // output results
    printf("Open lockers: ");
    for(l = 0; l < sizeof lockers; l++) {
        if(lockers[l]) {
            printf("%d ", l + 1);
        }
    }

    return 0;
}